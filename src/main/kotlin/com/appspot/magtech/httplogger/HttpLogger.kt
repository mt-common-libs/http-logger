package com.appspot.magtech.httplogger

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

enum class LogLevel {
    DEBUG, INFO, WARN, ERROR
}

class HttpLogger(
        var baseAddress: String
) : AutoCloseable {

    private val messagesQueue = Channel<String>()
    private val timeFormat = SimpleDateFormat("HH:mm:ss", Locale.US)
    private var isActive = true

    init {
        CoroutineScope(Dispatchers.IO).launch {
            sendMessage("\n--- New session (${timeFormat.format(Date())}) ---")
            while (isActive) {
                sendMessage(messagesQueue.receive())
            }
        }
    }

    fun log(level: LogLevel, text: String) {
        CoroutineScope(Dispatchers.IO).launch {
            messagesQueue.send("(${timeFormat.format(Date())}) $level: $text")
        }
    }

    fun debug(text: String) = log(LogLevel.DEBUG, text)
    fun info(text: String) = log(LogLevel.INFO, text)
    fun warning(text: String) = log(LogLevel.WARN, text)
    fun error(text: String) = log(LogLevel.ERROR, text)

    private suspend fun sendMessage(text: String) {
        try {
            val message = withContext(Dispatchers.IO) { URLEncoder.encode(text, "UTF-8") }
            val address =
                    withContext(Dispatchers.IO) { URL("${baseAddress}:3001/api/log?text=$message") }

            val httpConn =
                    withContext(Dispatchers.IO) { address.openConnection() as HttpURLConnection }
            httpConn.requestMethod = "POST"
            httpConn.connectTimeout = 5 * 1000
            httpConn.readTimeout = 5 * 1000

            val input = httpConn.inputStream
            withContext(Dispatchers.IO) { input.close() }
            httpConn.disconnect()
        } catch (exc: Exception) {
        }
    }

    override fun close() {
        isActive = false
    }
}
