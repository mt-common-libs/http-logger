plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.61")
    `java-library`
    `maven-publish`
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.0")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

publishing {
    repositories {
        maven("https://mymavenrepo.com/repo/2adloi83ZbzFqdzrVebr/")
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.appspot.magtech"
            artifactId = "http-logger"
            version = "0.1.0"

            from(components["kotlin"])
        }
    }
}

